Atlassian Gzip Filter
=========================

Atlassian Gzip Filter adds gzip filtering capabilities to your web application
transparently by using a servlet filter.

- [JIRA project](https://jira.atlassian.com/browse/GZIPFILTER)
- [SCM](https://bitbucket.org/atlassian/atlassian-gzipfilter)

Building
--------

Build the plugin from scratch and run the unit-tests:

    mvn clean package

Run unit and integration-tests:

    mvn verify

Usage
-----

The filter reacts on response and request headers to identify if response should be
compressed. Content-Encoding header is set only once on the first usage of response
PrintWriter or OutputStream.

Known Issues
------------

In Tomcat, when first output to the stream produced in included context
and the filter is not the first in the chain, Content-Encoding header can not be set
and response is not valid. Therefore, in applications producing first output from an
included context, the filter must be the first in the chain.


Releasing
---------

See https://engservices-bamboo.internal.atlassian.com/browse/GZIPFILT
