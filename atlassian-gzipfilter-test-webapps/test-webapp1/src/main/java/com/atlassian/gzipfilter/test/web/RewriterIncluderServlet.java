package com.atlassian.gzipfilter.test.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A small servlet that includes response
 */
public class RewriterIncluderServlet extends HttpServlet
{
    @Override
    protected void service(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException
    {
        // we must set headers here, because they will be ignored inside included servlet
        ContentTypeRewriter.applyContentTypes(req, resp);
        req.getRequestDispatcher("rewriter.html").include(req,resp);
    }
}
