package com.atlassian.gzipfilter.test.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;

/**
 * A test servlet that sends a 304 not modified, depending on the URL
 */
public class NotModifiedServlet extends HttpServlet
{
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setStatus(304);
        // response should have an no body
    }
}