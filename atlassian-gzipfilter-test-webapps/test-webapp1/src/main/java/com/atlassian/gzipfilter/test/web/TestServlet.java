package com.atlassian.gzipfilter.test.web;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * A test servlet
 */
public class TestServlet extends HttpServlet
{
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        final PrintWriter printWriter = resp.getWriter();
        String mimeType = req.getParameter("mimetype");
        resp.setContentType(mimeType != null ? mimeType : "text/html");
        printWriter.write("<h1>Test Servlet</h1>");
    }
}