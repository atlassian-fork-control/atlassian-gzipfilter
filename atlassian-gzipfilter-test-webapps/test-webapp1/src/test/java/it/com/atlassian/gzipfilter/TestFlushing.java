package it.com.atlassian.gzipfilter;

import junit.framework.TestCase;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.junit.Test;
import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/**
 * Test to ensure that an out.flush() in a servlet results in bytes being send to the browser
 */
public class TestFlushing
{

    @Test
    public void testFlushingWithNoFilter() throws Exception {
        doTimingTest(null, null);
    }
    @Test
    public void testFlushingWithGzFilter() throws Exception {
        doTimingTest(IntegrationTestUtils.GZIP_RESPONSE_HEADER, IntegrationTestUtils.GZIP_ACCEPT_HEADER);
    }

    private void doTimingTest(Header expectedContentEncodoing, Header acceptHeader) throws IOException {
        long pause = 4000;
        HttpClient client = new HttpClient();
        HttpMethod method = new GetMethod(IntegrationTestUtils.URL + "flushing.html?delay=" + pause);
        if (acceptHeader != null) {
            method.addRequestHeader(acceptHeader);
        }

        long t0 = System.currentTimeMillis();
        int rc = client.executeMethod(method);
        long t1 = System.currentTimeMillis();
        assertEquals("Should return 200 - success", 200, rc);
        assertEquals(expectedContentEncodoing, method.getResponseHeader("Content-Encoding"));
        InputStream response = method.getResponseBodyAsStream();
        if (acceptHeader != null) {
            response = new GZIPInputStream(response, 1);
        }

        expect("first", response);
        long t2 = System.currentTimeMillis();
        expect("second", response);
        long t3 = System.currentTimeMillis();
        expect("last", response);
        long t4 = System.currentTimeMillis();

        assertTrue("read 'first' in response took: " + (t2 - t0), (t2 - t0) < 100); // should get "first" within 100ms of making the request
        assertTimeWithin(pause, t2, t3); // should get "second" within 4000 of "first"
        assertTimeWithin(pause, t3, t4); // should get "last" within 4000 of "second"
    }

    private static void assertTimeWithin(long expectedDelay, long t0, long t1) {
        assertEquals(expectedDelay, t1 - t0, 80);
    }

    private static void expect(String expected, InputStream in) throws IOException {
        for (int i = 0; i < expected.length(); i++) {
            int a = expected.charAt(i);
            int b = in.read();
            assertEquals(a, b);
        }
    }
}
